$(function () {
    let myRadio = $("input[name=choix]");
    const key = 'dd8d7f8a1bde8e21377b059907544420';
    
    
    // let navbar = $('#navbarNav');
    // console.log(navbar);
    $('#submit').click(function () {
        let checkedValue = myRadio.filter(":checked").val(); //Film/Serie
        console.log(checkedValue);
        const searchTerm = $('#search').val();
        if(checkedValue == 'Film'){        
        // We empty the results list for a clean search each time ,on laisse vide la liste du  resultat pour une recherche clean a chaque fois
        $('#results').html(''); 
        // here We do the search ici on fait la recherche we call those movies
        $.get(`https://api.themoviedb.org/3/search/movie?api_key=${key}&query=${searchTerm}`, function (result, textStatus, jqXHR) {
            const movies = result.results;
            //this part For each element in our list cette partie pour chaque element dans notre liste
            movies.forEach(function (element) {
                 console.log(element);
                // We append the correct html code nous ajoutons le corresct htlm
                $('#results').append(
                    `<div class="col-md-3">
                      <div class="cinema text-center">
                        <p class="poster">
                            <img src="https://image.tmdb.org/t/p/w300_and_h450_bestv2/${element.poster_path}" alt="Image manquante pour le film ${element.title}" />
                        </p>
                        <p class="title">${element.title}</p>
                        <p class="overview">${element.overview}</p>
                        </div>
                    </div>
                    `
                );
            });
            // new way of writing a function just when you call it nouvelle methode d'ecrire une fonction just qd tu la declare(appel).
        }).done((results) => {
            // console.log('results', results);
        }).fail(function () {
            console.log('Il y a eu un probleme avec le api');
            // here's gonna be a second call for movie tv show
        });
        // Old method for concatenation lancienne methode de la concatenation
        // var kjqdshfiq = 'sdfsdfdsfdsf' + key;

    }//if
    else { //Serie
        $('#results').html(" ");
        // We empty the results list for a clean search each time ,on laisse vide la liste du  resultat pour une recherche clean a chaque fois

        // here We do the search ici on fait la recherche we call those movies
        $.get(`https://api.themoviedb.org/3/search/tv?api_key=${key}&query=${searchTerm}`, function (result, textStatus, jqXHR) {
            const movies = result.results;
            //this part For each element in our list cette partie pour chaque element dans notre liste
            movies.forEach(function (element) {
               // console.log(element);
                // We append the correct html code nous ajoutons le corresct htlm
                $('#results').append(
                    `<div class="movie">
                
                        <p class="poster">
                            <img src="https://image.tmdb.org/t/p/w400_and_h450_bestv2/${element.poster_path}" alt="Image manquante pour le film ${element.title}" />
                        </p>
                        <p class="title">${element.title}</p>
                        <p class="overview">${element.overview}</p>
                    </div>`
                );
            });
            // new way of writing a function just when you call it nouvelle methode d'ecrire une fonction just qd tu la declare(appel).
        }).done((results) => {
            // console.log('results', results);
        }).fail(function () {
            console.log('Il y a eu un probleme avec le api');
            // here's gonna be a second call for movie tv show
        });
        // Old method for concatenation lancienne methode de la concatenation
        // var kjqdshfiq = 'sdfsdfdsfdsf' + key;
    }
    });
});